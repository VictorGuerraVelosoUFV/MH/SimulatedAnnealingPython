'''
BSD 3-Clause License

Copyright (c) 2018, Victor Guerra Veloso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
from random import random, uniform
from math import exp, log
import math


class SimulatedAnnealing:
    def __init__(self, func, r):
        self.reset(func, r)

    def reset(self, func, r):  # r=((x1,x2)(y1,y2))
        self.func = func
        self.i = None
        self.x = r[0]
        self.y = r[1]
        self.c = [float()]
        self.L = [int()]

    def init_temp(self):
        E = []
        sum = 0
        for i in range(10):
            E.append(self.func())
        for i in range(9):
            sum += E[i + 1] - E[i]
        return (sum / 9) / log(0.8, math.e)

    def init(self, init_temp=0.5, L0=300):
        self.i = (uniform(self.x[0], self.x[1]), uniform(self.y[0], self.y[1]))
        self.c[0] = init_temp
        self.L[0] = L0

    def stop_criterion(self, k):
        return self.c[-1] <= 0.01 or k == 10000

    def tweak(self):
        while True:
            r = (uniform(self.i[0] * 0.95, self.i[0] * 1.05),
                 uniform(self.i[0] * 0.95, self.i[0] * 1.05))  # add noise of +/- 5%
            if self.x[0] <= r[0] <= self.x[1] and self.y[0] <= r[1] <= self.y[1]:  # test limits
                break
        return r

    def size_calc(self, k):
        self.L.append(int(math.ceil(self.L[k-1])))

    def control_calc(self, k):
        self.c.append(uniform(0.8, 0.99) * self.c[k-1])  # could fix it to 0.8

    def run(self):
        self.init()
        k = 0
        i = self.i
        while not self.stop_criterion(k):
            for l in range(self.L[k]):
                j = self.tweak()
                func_j = self.func(j[0], j[1])
                func_i = self.func(self.i[0], self.i[1])
                if func_j <= func_i or exp((func_i - func_j) / self.c[k]) > random():
                    self.i = j
            k += 1
            self.size_calc(k)
            self.control_calc(k)
        return self.c[-1]
